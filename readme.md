# matrix Qutebrowser Theme

A theme for [Qutebrowser](https://qutebrowser.org) based on the [Matrix](https://www.schemecolor.com/matrix-code-green.php) theme.

## Install

1. Clone this repo or copy `matrix-qutebrowser.py`.
2. Symlink (or just copy) the file `matrix-qutebrowser.py` to your `.qutebrowser` directory.
3. Add `config.source('matrix-qutebrowser.py')` at the _end_ of your `config.py` file.
